<?php
    session_start();
    session_unset();
    session_destroy();
    session_start();
    $_SESSION["login"] = false;
    echo ("<script type='text/javascript'>
    location.href='index.php';
    </script>");
?>