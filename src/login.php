<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php
        session_start();
        session_unset();
        session_destroy();

        session_start();
        $_SESSION["login"] = false;
        if (!empty($_POST['username'])) {
            require_once "mysqli_conn.php";
            //Check email and password
            $sql = "SELECT * FROM userinfo where user_name='{$_POST["username"]}' AND user_password='{$_POST["password"]}'";
            $rs = mysqli_query($conn, $sql) or die(mysqli_error($conn));
            if (mysqli_num_rows($rs)>0) {
                while($rc=mysqli_fetch_assoc($rs)){
                    $sql = "SELECT * FROM userinfo WHERE user_name ='".$_POST['username']."'";
                    $rs = mysqli_query($conn, $sql);
                    $rc = mysqli_fetch_assoc($rs);
                    $_SESSION["userid"] = $rc['user_id'];
                    $_SESSION["login"] = true;
                    $_SESSION["username"]=$_POST["username"];
                    echo ("<script type='text/javascript'>               
                           location.href='index.php';
                       </script>");
                }
            } else {
                echo ("<script type='text/javascript'>
                   alert('username or password is incorrect!');
               </script>");
            }
            
        }
    ?>
    

    <!-- Website Title -->
    <title>Hong Kong Beach Website</title>
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="css/swiper.css" rel="stylesheet">
	<link href="css/magnific-popup.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	

</head>
<body data-spy="scroll" data-target=".fixed-top">
    
    

    <!-- Navbar -->
    <nav class="navbar navbar-expand-md navbar-dark navbar-custom fixed-top">
        <!-- Text Logo - Use this if you don't have a graphic logo -->
        <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Beach</a> -->

        <!-- Image Logo -->
        <a class="navbar-brand logo-image" href="index.php"><img src="images/logo.png" alt="alternative"></a>
        

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="index.php">HOME <span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link page-scroll" href="register.php">REGISTER</a>
                </li>
            </ul>
        </div>
    </nav> <!-- end of navbar -->
    <!-- end of navbar -->


    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12" align="center">
                        <div class="text-container">
                        <form method="post" action="login.php">
                        <table>
                        <tr><td style="padding:15px;" align="center">Username:</td><td align="center"> <input type="text" id="username" name="username"></td></tr>
                        <tr><td style="padding:15px;" align="center">Password:</td><td align="center"> <input type="password" id="password" name="password"></td></tr>
                        </table>
                        <input type="submit" value="Login" class="btn-solid-lg page-scroll" style="padding:15px;">
                        </form>
                        </div>
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
    </header> <!-- end of header -->
    <!-- end of header -->



    
    <!-- Scripts -->
    <script src="js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/morphext.min.js"></script> <!-- Morphtext rotating text in the header -->

</body>
</html>