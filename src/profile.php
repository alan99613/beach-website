<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php
        function checkPassword($fPassword, $sPassword) {
            return $fPassword == $sPassword;
        }
        if (!empty($_POST['Npassword'])) {
            session_start();
            require_once "mysqli_conn.php";
            $a = $_SESSION['username'];
            //Check email and password
            $sql2 = "SELECT user_password FROM userinfo WHERE user_name = '".$a."'";
            $rc = mysqli_query($conn, $sql2);
            while ($rd = mysqli_fetch_array($rc)){
                if(!checkPassword($_POST["Npassword"],$_POST["RNpassword"])) {
                    echo ("<script type='text/javascript'>     
                    alert('new password and repeat new password a not same');      
                    location.href='profile.php';      
                    </script>");
                }else if (!checkPassword($rd['user_password'],$_POST["Opassword"])){
                    echo ("<script type='text/javascript'>     
                    alert('Old password not correct');    
                    location.href='profile.php';                
                    </script>");
                }else{
                    $sql = "UPDATE userinfo SET user_password = '{$_POST["Npassword"]}' WHERE user_name = '".$a."'";
                    $rs = mysqli_query($conn, $sql);
                    if ($rs)
                        echo ("<script type='text/javascript'>     
                        alert('success');                   
                        location.href='index.php';
                        </script>");
                }
            }
        }
    ?>
    

    <!-- Website Title -->
    <title>Hong Kong Beach Website</title>
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="css/swiper.css" rel="stylesheet">
	<link href="css/magnific-popup.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	

</head>
<body data-spy="scroll" data-target=".fixed-top">
    
    

    <!-- Navbar -->
    <nav class="navbar navbar-expand-md navbar-dark navbar-custom fixed-top">
        <!-- Text Logo - Use this if you don't have a graphic logo -->
        <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Beach</a> -->

        <!-- Image Logo -->
        <a class="navbar-brand logo-image" href="index.php"><img src="images/logo.png" alt="alternative"></a>
        

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="index.php">HOME <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                <?php
                session_start();
                if($_SESSION['login'] != true){
                    echo "<a class='nav-link page-scroll' href='login.php'>LOGIN</a>"; 
                    echo "</li>
                    <li class='nav-item'>
                    <a class='nav-link page-scroll' href='register.php'>REGISTER</a>";
                }else{
                    echo "<a class='nav-link page-scroll' href='history.php'>HISTORY</a>";
                    echo "</li>
                        <li class='nav-item'>
                        <a class='nav-link page-scroll' href='favorite.php'>FAVORITE</a></li>";

                    echo "<li class='nav-item'><a class='nav-link page-scroll' href='profile.php'>PROFILE</a>";
                    echo "</li>
                        <li class='nav-item'>
                        <a class='nav-link page-scroll' href='logout.php'>LOGOUT</a>";
                }
	
                ?>
                </li>
            </ul>
        </div>
    </nav> <!-- end of navbar -->
    <!-- end of navbar -->


    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12" align="center">
                        <div class="text-container">
                        <form method="post" action="profile.php">
                        <table style="color: white;">
                        <tr><td style="padding:15px;" align="center">Old Password:</td><td align="center"> <input type="password" id="Opassword" name="Opassword"></td></tr>
                        <tr><td style="padding:15px;" align="center">New Password:</td><td align="center"> <input type="password" id="Npassword" name="Npassword"></td></tr>
                        <tr><td style="padding:15px;" align="center">Repeat New Password:</td><td align="center"> <input type="password" id="RNpassword" name="RNpassword"></td></tr>
                        </table>
                        <input type="submit" value="Change">
                        </form>
                        </div>
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
    </header> <!-- end of header -->
    <!-- end of header -->



    
    <!-- Scripts -->
    <script src="js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/morphext.min.js"></script> <!-- Morphtext rotating text in the header -->

</body>
</html>