<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <?php
        if (empty($_GET['beachid'])) {
            return;
        }
        require_once ("mysqli_conn.php");
        require_once ("Import_Weather_Info.php");
        $sql = "SELECT * FROM beachinfo WHERE beach_id ='".$_GET['beachid']."'";
        $rs = mysqli_query($conn,$sql);
        $rc = mysqli_fetch_assoc($rs);
    ?>
    <?php
        function getBeachMapInfo($rc) {
            $array['beach_name'] = "'".addslashes($rc['beach_name'])."'";
            $array['address'] = "'".$rc['address'].", ".ucwords(strtolower($rc['district']))."'";
            $array['lat'] = $rc['lat'];
            $array['lng'] = $rc['lng'];
            return $array;
        }
        $mapInfo = getBeachMapInfo($rc);
    ?>
    <head>
    	<!-- NAME: 1 COLUMN -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Hong Kong Beach Website</title>
        <script defer
            src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap">
        </script>
        <script>
            let map, marker;
            var beach = { lat: <?php echo $mapInfo['lat'] ?>, lng: <?php echo $mapInfo['lng']?> };
            var beachName = <?php echo $mapInfo['beach_name'] ?>;
            var beachAddress = <?php echo $mapInfo['address'] ?>;

            function initMap() {
                map = new google.maps.Map(document.getElementById("map"), {
                    center: beach,
                    zoom: 15,
                });

                marker = new google.maps.Marker({
                    position: beach,
                    map: map,
                });
                
                var infoWindow = new google.maps.InfoWindow;
                var infowincontent = document.createElement('div');
                var strong = document.createElement('strong');
                strong.textContent = beachName;
                infowincontent.appendChild(strong);
                infowincontent.appendChild(document.createElement('br'));
                var text = document.createElement('text');
                text.textContent = beachAddress;
                infowincontent.appendChild(text);
                marker.addListener('click', function() {
                    infoWindow.setContent(infowincontent);
                    infoWindow.open(map, marker);
                });
            }
        </script>
</head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN PREHEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templatePreheader">
                                        <tr>
                                        	<td valign="top" class="preheaderContainer" style="padding-top:9px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="TextBlock">
  
</table></td>
                                        </tr>
                                    </table>
                                    <!-- // END PREHEADER -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                                        <tr>
                                            <td valign="top" class="headerContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="DividerBlock">
    <tbody class="DividerBlockOuter">
        <tr>
            <td class="DividerBlockInner" style="padding: 10px 18px;">
                <table class="DividerContent" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="DividerBlock">
    <tbody class="DividerBlockOuter">
        <tr>
            <td class="DividerBlockInner" style="padding: 10px 18px;">
                <table class="DividerContent" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="ImageBlock">
    <tbody class="ImageBlockOuter">
            <tr>
                <td valign="top" style="padding:0px" class="ImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="ImageContentContainer">
                        <tbody><tr>
                            <td class="ImageContent" valign="top" style="padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0;">
                                
                                        <img align="left" alt="" src="images/<?php echo $_GET['beachid']?>.jpg" width="600" style="max-width:1200px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="Image">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="DividerBlock">
    <tbody class="DividerBlockOuter">
        <tr>
            <td class="DividerBlockInner" style="padding: 20px 18px;">
                <table class="DividerContent" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                                        <tr>
                                            <td valign="top" class="bodyContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="TextBlock">
    <tbody class="TextBlockOuter">
        <tr>
            <td valign="top" class="TextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="TextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="TextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <div style="text-align: center;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:39px"><span style="color:#00bcc8"><strong><?php echo $rc['beach_name']?></strong></span></span><br>
<br>
<span style="color:#697b7c"><span style="font-size:14px; line-height:14px; text-align:justify"><?php echo $rc['beach_info'];?></span></span></span></div>

                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="DividerBlock">
    <tbody class="DividerBlockOuter">
        <tr>
            <td class="DividerBlockInner" style="padding: 20px 18px 0px;">
                <table class="DividerContent" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="ImageBlock">
    <tbody class="ImageBlockOuter">
            <tr>
                <td valign="top" style="padding:0px" class="ImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="ImageContentContainer">
                        <tbody><tr>
                            <td class="ImageContent" valign="top" style="padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0;">
                                
                                    
                                        <img align="left" alt="" src="images/line.jpg" width="600" style="max-width:1200px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="Image">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="CaptionBlock">
    <tbody class="CaptionBlockOuter">
        <tr>
            <td class="CaptionBlockInner" valign="top" style="padding:9px;">
                



<table border="0" cellpadding="0" cellspacing="0" class="CaptionRightContentOuter" width="100%">
    <tbody><tr>
        <td valign="top" class="CaptionRightContentInner" style="padding:0 9px ;">
            <table align="left" border="0" cellpadding="0" cellspacing="0" class="CaptionRightImageContentContainer">
                <tbody><tr>
                    <td class="CaptionRightImageContent" valign="top">
                    
                        

                        <img alt="" src="images/weather.jpg" width="132" style="max-width:268px;" class="Image">
                        

                    
                    </td>
                </tr>
            </tbody></table>
            <table class="CaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="396">
                <tbody><tr>
                    <td valign="top" class="TextContent" style="text-align: left;">
                        <span style="font-size:24px"><strong><span style="color:#00bcc8">Weather</span></strong></span><br>
<br>
<span style="color:#697b7c"><?php echo "Temperature: ".$rc['temperature']; echo "<br/>"; echo "Rainfall: ".$rc['rainfall'];?></span>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>




            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="DividerBlock">
    <tbody class="DividerBlockOuter">
        <tr>
            <td class="DividerBlockInner" style="padding: 20px 18px;">
                <table class="DividerContent" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="CaptionBlock">
    <tbody class="CaptionBlockOuter">
        <tr>
            <td class="CaptionBlockInner" valign="top" style="padding:9px;">
                



<table border="0" cellpadding="0" cellspacing="0" class="CaptionRightContentOuter" width="100%">
    <tbody><tr>
        <td valign="top" class="CaptionRightContentInner" style="padding:0 9px ;">
            <table align="left" border="0" cellpadding="0" cellspacing="0" class="CaptionRightImageContentContainer">
                <tbody><tr>
                    <td class="CaptionRightImageContent" valign="top">
                    
                        

                        <img alt="" src="images/service.jpg" width="132" style="max-width:268px;" class="Image">
                        

                    
                    </td>
                </tr>
            </tbody></table>
            <table class="CaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="396">
                <tbody><tr>
                    <td valign="top" class="TextContent" style="text-align: left;">
                        <span style="font-size:24px"><strong><span style="color:#00bcc8">Public facilities</span></strong></span><br>
<br>
<span style="color:#697b7c"><?php echo $rc['facility'];?></span>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>




            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="DividerBlock">
    <tbody class="DividerBlockOuter">
        <tr>
            <td class="DividerBlockInner" style="padding: 20px 18px;">
                <table class="DividerContent" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="CaptionBlock">
    <tbody class="CaptionBlockOuter">
        <tr>
            <td class="CaptionBlockInner" valign="top" style="padding:9px;">
                



<table border="0" cellpadding="0" cellspacing="0" class="CaptionRightContentOuter" width="100%">
    <tbody><tr>
        <td valign="top" class="CaptionRightContentInner" style="padding:0 9px ;">
            <table align="left" border="0" cellpadding="0" cellspacing="0" class="CaptionRightImageContentContainer">
                <tbody><tr>
                    <td class="CaptionRightImageContent" valign="top">
                    
                        

                        <img alt="" src="images/water.jpg" width="132" style="max-width:268px;" class="Image">
                        

                    
                    </td>
                </tr>
            </tbody></table>
            <table class="CaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="396">
                <tbody><tr>
                    <td valign="top" class="TextContent">
                        <span style="font-size:24px"><strong><span style="color:#00bcc8">Water quality</span></strong></span><br>
<br>
<span style="color:#697b7c"><?php echo $rc['water_quality'];?></span>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>




            </td>
        </tr>
    </tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="100%" class="CaptionBlock">
    <tbody class="CaptionBlockOuter">
        <tr>
            <td class="CaptionBlockInner" valign="top" style="padding:9px;">
                <table border="0" cellpadding="0" cellspacing="0" class="CaptionRightContentOuter" width="100%">
                    <tbody>
                        <tr>
                            <td valign="top" class="CaptionRightContentInner" style="padding:0 9px;">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="CaptionRightImageContentContainer">
                                    <tbody>
                                        <tr>
                                            <td class="CaptionRightImageContent" valign="top"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="CaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="396">
                                    <tbody>
                                        <tr>
                                            <td valign="top" class="TextContent" style="text-align: left;">
                                                <span style="font-size:24px"><strong><span style="color:#00bcc8">Lifeguard</span></strong></span><br><br>
                                                <span style="color:#697b7c"><?php if ($rc['lifeguard'] == 1) echo "Yes"; else echo "No"; ?></span><br><br>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="TextContent" style="text-align: left;">
                                                <span style="font-size:24px"><strong><span style="color:#00bcc8">Attendance last year</span></strong></span><br><br>
                                                <span style="color:#697b7c"><?php echo $rc['attendance_last_year']; ?></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="100%" class="ImageBlock">
    <tbody class="ImageBlockOuter">
            <tr>
                <td valign="top" style="padding:9px" class="ImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="ImageContentContainer">
                        <tbody><tr>
                            <td class="ImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0;">
                                
                                    
                                        <img align="left" alt="" src="images/line.jpg" width="564" style="max-width:1200px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="Image">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="TextBlock">
    <tbody class="TextBlockOuter">
        <tr>
            <td valign="top" class="TextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="TextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="TextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <div style="text-align: center;"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:39px"><span style="color:#00bcc8"><strong>Check location</strong></span></span><br>
<br>
<span style="color:#697b7c"><span style="font-size:14px; line-height:14px; text-align:justify">
	Location:
</span></span></span><div id="map" style="height: 500px;"></div></div>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="DividerBlock">
    <tbody class="DividerBlockOuter">
        <tr>
            <td class="DividerBlockInner" style="padding: 20px 18px 0px;">
                <table class="DividerContent" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="ButtonBlock">
    <tbody class="ButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="center" class="ButtonBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" class="ButtonContentContainer" style="border-collapse: separate !important;border-radius: 5px;background-color: #00BCC8;">
                    <tbody>
                        <tr>
                            <td align="center" valign="middle" class="ButtonContent" style="font-family: Arial; font-size: 16px; padding: 16px;">
                                <a class="Button" title="Yes, I am interested" href="https://www.google.com/maps/@<?php echo $rc['lat']?>,<?php echo $rc['lng']?>,15z" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Yes, I want to go on</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="ImageBlock">
    <tbody class="ImageBlockOuter">
            <tr>
                <td valign="top" style="padding:9px" class="ImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="ImageContentContainer">
                        <tbody><tr>
                            <td class="ImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0;">
                                
                                    
                                        <img align="left" alt="" src="images/line.jpg" width="564" style="max-width:1200px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="Image">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="ButtonBlock">
    <tbody class="ButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="center" class="ButtonBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" class="ButtonContentContainer" style="border-collapse: separate !important;border-radius: 5px;background-color: #00BCC8;">
                    <tbody>
                        <tr>
                            <td align="center" valign="middle" class="ButtonContent" style="font-family: Arial; font-size: 16px; padding: 16px;">
                                <a class="Button" href="index.php" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Home</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                           
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>
