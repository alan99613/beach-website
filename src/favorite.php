<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    

    <!-- Website Title -->
    <title>Hong Kong Beach Website</title>
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="css/swiper.css" rel="stylesheet">
	<link href="css/magnific-popup.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target=".fixed-top">
    
    

    <!-- Navbar -->
    <nav class="navbar navbar-expand-md navbar-dark navbar-custom fixed-top">
        <!-- Text Logo - Use this if you don't have a graphic logo -->
        <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Beach</a> -->

        <!-- Image Logo -->
        <a class="navbar-brand logo-image" href="index.php"><img src="images/logo.png" alt="alternative"></a>
        

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="index.php">HOME <span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item">
                <?php
                    session_start();
                    if($_SESSION['login'] != true){
                        echo "<a class='nav-link page-scroll' href='login.php'>LOGIN</a>"; 
                        echo "</li>
                        <li class='nav-item'>
                        <a class='nav-link page-scroll' href='register.php'>REGISTER</a>";
                    }else{
                        echo "<a class='nav-link page-scroll' href='history.php'>HISTORY</a>";
                        echo "</li>
                            <li class='nav-item'>
                            <a class='nav-link page-scroll' href='favorite.php'>FAVORITE</a></li>";

                        echo "<li class='nav-item'><a class='nav-link page-scroll' href='profile.php'>PROFILE</a>";
                        echo "</li>
                            <li class='nav-item'>
                            <a class='nav-link page-scroll' href='logout.php'>LOGOUT</a>";
                    }
	
                ?>
                </li>
            </ul>
        </div>
    </nav> <!-- end of navbar -->
    <!-- end of navbar -->


    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-container">
                            <h1>Your Favorite</h1>
                            <p class="p-heading p-large">Website Introduction</p>
                            <a class="btn-solid-lg page-scroll" href="#HKIsland">DISCOVER</a>
                        </div>
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
    </header> <!-- end of header -->
    <!-- end of header -->

    <!-- Hong Kong Island -->
    <div id="HKIsland" class="counter">
        <div class="container">
            <div class="area-title">FAVORITE</div><hr />

            
    <?php 
        require_once ("mysqli_conn.php");
        $sql = "SELECT * FROM bookmark WHERE user_id  = '".$_SESSION['userid']."'";
        $rs = mysqli_query($conn,$sql);
        while ($rc = mysqli_fetch_array($rs)){
            $sql2 = "SELECT * FROM beachinfo WHERE beach_id  = '".$rc['beach_id']."'";
            $rs2 = mysqli_query($conn,$sql2);
            while ($rc2 = mysqli_fetch_array($rs2)){
                echo "<div class='row'>
                    <div class='col-lg-5 col-xl-6'>
                    <div class='image-container'>
                    <img class='img-fluid' src='images/".$rc2['beach_id'].".jpg' alt='alternative'>
                    </div> <!-- end of image-container -->
                    </div> <!-- end of col -->
                    <div class='col-lg-7 col-xl-6'>
                    <div class='text-container'>";
                echo "<h2>".$rc2['beach_name']."</h2>";

                echo "<ul class='list-unstyled li-space-lg'>
                    <li class='media'>
                    <i class='fas fa-square'></i>";
                echo "<div class='media-body'>".$rc2['beach_info']."</div>";
                echo "</li><li class='media'>
                    <i class='fas fa-square'></i>";
                echo "<div class='media-body'>Address :".$rc2['address']."</div>";

                echo "</li><li class='media'>
                    <i class='fas fa-square'></i>";
                echo "<div class='media-body'>
                    <a href=detailbeach.php?beachid=".$rc2['beach_id'].">Learn more</a><br/>";
                if($_SESSION['login'] == true){
                    $sql3  = "SELECT * FROM bookmark WHERE user_id = '".$_SESSION['userid']."'";
                    $rs3 = mysqli_query($conn,$sql3);
                    while ($rc3 = mysqli_fetch_array($rs3)){
                        if ($rc3['beach_id'] == $rc['beach_id']){
                            $test = true;
                            echo "<a href=removefavorite.php?beachid=".$rc['beach_id']."&userid=".$_SESSION['userid'].">Remove Favorite</a><br/>";
                        }
                    }
                    if($test == false){
                        echo "<a href=addfavorite.php?beachid=".$rc['beach_id']."&userid=".$_SESSION['userid'].">Add To Favorite</a><br/>";
                    }
                    echo "<a href=addhistory.php?beachid=".$rc['beach_id']."&userid=".$_SESSION['userid'].">My History</a>";
                }
                echo  "</div> 
                    </li>
                    </ul>
                    </div> <!-- end of text-container -->      
                    </div> <!-- end of col -->
                    </div><br /> <!-- end of row -->";
            }
        }
    ?>

        </div> <!-- end of container -->
    </div> <!-- end of counter -->
    <!-- end of Hong Kong Island -->

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/morphext.min.js"></script> <!-- Morphtext rotating text in the header -->
    <script src="js/scripts.js"></script> <!-- Custom scripts -->
</body>
</html>