<?php    

    $url = "https://data.weather.gov.hk/weatherAPI/opendata/weather.php?dataType=rhrread&lang=en";
    
    $data = file_get_contents($url);   // put the contents of the file into a variable
    $characters = json_decode($data);

    $arrayRainData = $characters->rainfall->data;
    $arrayTemperatureData = $characters->temperature->data;

    foreach($arrayRainData as $jsObject){
        $rainfall = $jsObject->max . $jsObject->unit;
        $districtString = $jsObject->place ;
        $district = str_replace(" District", "" , $districtString);


        $sql = 'UPDATE beachinfo SET rainfall="' . $rainfall . '" WHERE district = "'. $district .'" ';

        if ($conn->query($sql) === TRUE) {
            //echo "Record updated successfully";
        } else {
            //echo "Error updating record: " . $conn->error;
        }

    }

    foreach($arrayTemperatureData as $jsObject){
        $temperature = $jsObject->value . $jsObject->unit;
        $district = $jsObject->place ;
        $pos = strpos(  $district , "Tsuen Wan");

        if($pos === 0){
            $district = "TSUEN WAN";
        }

        $sql = 'UPDATE beachinfo SET temperature="' . $temperature . '" WHERE district LIKE "%'. $district .'%"';
        if ($conn->query($sql) === TRUE) {
            //echo "Record updated successfully";
            //echo $sql . "";
        } else {
            //echo "Error updating record: " . $conn->error;
        }

    }

?>
