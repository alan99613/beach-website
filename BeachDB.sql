-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2021 at 09:11 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beachdb`
--
CREATE DATABASE IF NOT EXISTS `beachdb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `beachdb`;

-- --------------------------------------------------------

--
-- Table structure for table `beachinfo`
--

CREATE TABLE `beachinfo` (
  `beach_id` int(11) NOT NULL,
  `beach_name` varchar(255) DEFAULT NULL,
  `beach_info` varchar(1000) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `category` int(11) NOT NULL,
  `district` varchar(255) DEFAULT NULL,
  `facility` varchar(255) DEFAULT NULL,
  `water_quality` varchar(255) DEFAULT NULL,
  `temperature` varchar(255) DEFAULT NULL,
  `rainfall` varchar(255) DEFAULT NULL,
  `path_comment` varchar(255) DEFAULT NULL,
  `lat` float(10,6) DEFAULT NULL,
  `lng` float(10,6) DEFAULT NULL,
  `attendance_last_year` int(11) DEFAULT NULL,
  `lifeguard` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `beachinfo`
--

INSERT INTO `beachinfo` (`beach_id`, `beach_name`, `beach_info`, `address`, `category`, `district`, `facility`, `water_quality`, `temperature`, `rainfall`, `path_comment`, `lat`, `lng`, `attendance_last_year`, `lifeguard`) VALUES
(1, 'Anglers\' Beach', 'In recent years, there is a trend of gradual improvement attributed to the government’s effort in providing sewerage along the coastal strip between Tsing Lung Tau and Ting Kau areas in phases, and the commissioning of the Sham Tseng Sewage Treatment Works and the Disinfection Facilities at Stonecutters Island Sewage Treatment Works.', '13 milestone, Castle Peak Road - Sham Tseng, Tsuen Wan', 2, 'TSUEN WAN', 'BEACH,BARBECUE SITES', '1', '23C', '0mm', NULL, 22.364876, 114.055931, 89990, 1),
(2, 'Approach Beach', 'In recent years, there is a trend of gradual improvement attributed to the government’s effort in providing sewerage along the coastal strip between Tsing Lung Tau and Ting Kau areas in phases, and the commissioning of the Sham Tseng Sewage Treatment Works and the Disinfection Facilities at Stonecutters Island Sewage Treatment Works. ', '10½ milestone, Castle Peak Road - Ting Kau, Tsuen Wan', 2, 'TSUEN WAN', 'BEACH,BARBECUE SITES', '1', '23C', '0mm', NULL, 22.367407, 114.086441, 33285, 1),
(3, 'Big Wave Bay Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Big Wave Bay Road, Shek O', 1, 'SOUTHERN', 'BEACH,BARBECUE SITES', '1', '19C', '0mm', NULL, 22.246574, 114.247162, 92430, 1),
(4, 'Butterfly Beach', 'The annual ranking of this beach has been maintained as “Fair” since 1998. During a bathing season, it mostly achieves a weekly rating of Grade 2, while it also hovers to Grade 1 or Grade 3.\r\n \r\nDuring and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Lung Mun Road, Tuen Mun', 2, 'TUEN MUN', 'BEACH', '1', '21C', '0mm', NULL, 22.372763, 113.956482, 335550, 1),
(5, 'Cafeteria New Beach', 'The annual ranking of this beach has been maintained as “Fair” since 1998, and received “Good” ranking in 2018. During a bathing season, it usually achieves Grade 2, while sometimes it hovers to Grade 1.\r\n \r\nDuring and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', '18½ milestone, Castle Peak Road- Castle Peak Bay, Tuen Mun', 2, 'TUEN MUN', 'BEACH', '1', '21C', '0mm', NULL, 22.373549, 113.986755, 357930, 1),
(6, 'Cafeteria Old Beach', 'The annual ranking of this beach has been maintained as “Fair” since 1998. During a bathing season, it mostly achieves a weekly rating of Grade 2, while occasionally it hovers to Grade 1 or Grade 3.\r\n \r\nDuring and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', '18¾ milestone, Castle Peak Road- Castle Peak Bay, Tuen Mun', 2, 'TUEN MUN', 'BEACH,BARBECUE SITES', '1', '21C', '0mm', NULL, 22.375013, 113.984596, 365650, 1),
(7, 'Casam Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', '11½ milestone, Castle Peak Road - Ting Kau, Tsuen Wan', 2, 'TSUEN WAN', 'BEACH', '1', '23C', '0mm', NULL, 22.367588, 114.074905, 18190, 1),
(8, 'Castle Peak Beach', 'The annual ranking of this beach has been maintained as “Fair” since 1999. It was closed to swimmers for over 20 years and has been re-opened since 2005. In a bathing season, it now usually achieves a weekly rating of Grade 2, while sometimes it improves to Grade 1.\r\n \r\nDuring and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', '19 milestone, Castle Peak Road- Castle Peak Bay, Tuen Mun', 2, 'TUEN MUN', 'BEACH,BARBECUE SITES', '1', '21C', '0mm', NULL, 22.379623, 113.979416, 260730, 1),
(9, 'Cheung Chau Tung Wan Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Cheung Chau Beach Road, Cheung Chau', 0, 'ISLANDS', 'BEACH', '1', '19C', '0mm', NULL, 22.210413, 114.029922, 286310, 1),
(10, 'Chung Hom Kok Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Chung Hom Kok Road, Chung Hom Kok', 0, 'SOUTHERN', 'BEACH,BARBECUE SITES', '1', '19C', '0mm', NULL, 22.218025, 114.202103, 83955, 1),
(11, 'Clear Water Bay First Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Tai Wan Tau, Clear Water Bay Road, Sai Kung', 2, 'SAI KUNG', 'BEACH,BARBECUE SITES', '1', '19C', '0mm', NULL, 22.291119, 114.291084, 94325, 1),
(12, 'Clear Water Bay Second Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Tai O Mun Road, Sai Kung', 3, 'SAI KUNG', 'BEACH', '1', '19C', '0mm', NULL, 22.288155, 114.287636, 708130, 1),
(13, 'Deep Water Bay Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Island Road, Deep Water Bay', 1, 'SOUTHERN', 'BEACH,BARBECUE SITES', '2', '19C', '0mm', NULL, 22.245010, 114.187149, 606530, 1),
(14, 'Gemini Beaches', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', '12 milestone, Castle Peak Road - Sham Tseng, Tsuen Wan', 2, 'TSUEN WAN', 'BEACH,BARBECUE SITES', '1', '23C', '0mm', NULL, 22.363962, 114.069214, 12220, 1),
(15, 'Golden Beach', 'Beach water quality monitoring at Golden Beach began in 1995. The annual ranking of this beach has been maintained as “Fair” since 1998, and received “Good” ranking in 2018. During a bathing season, it usually achieves Grade 2, while sometimes it hovers to Grade 1 or Grade 3.\r\n \r\nDuring and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', '18½ Milestone, Castle Peak Road- Castle Peak Bay,Tuen Mun', 2, 'TUEN MUN', 'BEACH', '1', '21C', '0mm', NULL, 22.372496, 113.987869, 927950, 1),
(16, 'Hair Pin Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Stanley Beach Road, Stanley', 1, 'SOUTHERN', 'BEACH,BARBECUE SITES', '2', '19C', '0mm', NULL, 22.224022, 114.214882, 61180, 0),
(17, 'Hap Mun Bay Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Sharp Island, Sai Kung', 2, 'SAI KUNG', 'BEACH,BARBECUE SITES', '1', '19C', '0mm', NULL, 22.356541, 114.296036, 92466, 1),
(18, 'Hoi Mei Wan Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', '11¾ milestone, Castle Peak Road - Ting Kau, Tsuen Wan', 2, 'TSUEN WAN', 'BEACH,BARBECUE SITES', '1', '23C', '0mm', NULL, 22.364864, 114.070984, 24740, 0),
(19, 'Hung Shing Yeh Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Yung Shue Wan, Lamma Island', 3, 'ISLANDS', 'BEACH,BARBECUE SITES', '2', '19C', '0mm', NULL, 22.218657, 114.119804, 273830, 1),
(20, 'Kadoorie Beach', 'The annual ranking of this beach has been maintained as “Fair” since 1998. During a bathing season, it usually achieves Grade 2, while sometimes it hovers to other Grades.\r\n \r\nDuring and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', '18¾ milestone, Castle Peak Road- Castle Peak Bay, Tuen Mun', 2, 'TUEN MUN', 'BEACH,BARBECUE SITES', '1', '21C', '0mm', NULL, 22.376581, 113.981415, 223240, 1),
(21, 'Kiu Tsui Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Kiu Tsui, Sharp Island, Sai Kung', 2, 'SAI KUNG', 'BEACH,BARBECUE SITES', '1', '19C', '0mm', NULL, 22.368286, 114.288849, 87522, 1),
(22, 'Kwun Yam Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Hak Pai Road, Cheung Chau', 3, 'ISLANDS', 'BEACH', '3', '19C', '0mm', NULL, 22.207029, 114.034554, 92730, 1),
(23, 'Lido Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', '11½ milestone, Castle Peak Road - Ting Kau, Tsuen Wan', 2, 'TSUEN WAN', 'BEACH', '1', '23C', '0mm', NULL, 22.367832, 114.076118, 182480, 1),
(24, 'Lo So Shing Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Sok Kwu Wan, Lamma Island', 3, 'ISLANDS', 'BEACH,BARBECUE SITES', '1', '19C', '0mm', NULL, 22.204411, 114.122673, 70230, 1),
(25, 'Lower Cheung Sha Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'South Lantau Road, Lantau Island', 3, 'ISLANDS', 'BEACH', '2', '19C', '0mm', NULL, 22.232321, 113.945374, 184910, 1),
(26, 'Ma Wan Tung Wan Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Ma Wan Tung Wan, Tsuen Wan', 3, 'TSUEN WAN', 'BEACH', '1', '23C', '0mm', NULL, 22.349993, 114.061668, 211930, 1),
(27, 'Middle Bay Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'South Bay Road, Repulse Bay', 1, 'SOUTHERN', 'BEACH,BARBECUE SITES', '2', '19C', '0mm', NULL, 22.229700, 114.198090, 50795, 1),
(28, 'Pui O Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Pui O, Lantau Island', 3, 'ISLANDS', 'BEACH,BARBECUE SITES', '1', '19C', '0mm', NULL, 22.239712, 113.976807, 433550, 1),
(29, 'Repulse Bay Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Beach Road, Repulse Bay', 1, 'SOUTHERN', 'BEACH', '3', '19C', '0mm', NULL, 22.236406, 114.196335, 4061595, 1),
(30, 'Rocky Bay Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Shek O Road, Shek O', 1, 'SOUTHERN', 'BEACH', '1', '19C', '0mm', NULL, 22.231709, 114.251762, 0, 0),
(31, 'Shek O Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Shek O Road, Shek O', 1, 'SOUTHERN', 'BEACH,BARBECUE SITES', '1', '19C', '0mm', NULL, 22.228977, 114.251122, 935200, 1),
(32, 'Silver Mine Bay Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Tong Wan Tau Road, Mui Wo, Lantau Island', 3, 'ISLANDS', 'BEACH,BARBECUE SITES', '1', '19C', '0mm', NULL, 22.269426, 113.998734, 146205, 1),
(33, 'Silverstrand Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Clear Water Bay Road, Sai Kung', 2, 'SAI KUNG', 'BEACH', '1', '19C', '0mm', NULL, 22.322653, 114.272163, 96375, 1),
(34, 'South Bay Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'South Bay Road, Repulse Bay', 1, 'SOUTHERN', 'BEACH,BARBECUE SITES', '2', '19C', '0mm', NULL, 22.224960, 114.197662, 45750, 1),
(35, 'St. Stephen\'s Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Wong Ma Kok Path, Stanley', 1, 'SOUTHERN', 'BEACH,BARBECUE SITES', '2', '19C', '0mm', NULL, 22.211845, 114.214653, 77710, 1),
(36, 'Stanley Main Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Stanley Beach Road, Stanley', 1, 'SOUTHERN', 'BEACH,BARBECUE SITES', '1', '19C', '0mm', NULL, 22.221317, 114.213959, 374500, 1),
(37, 'Ting Kau Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', '11 milestone, Castle Peak Road - Ting Kau, Tsuen Wan', 2, 'TSUEN WAN', 'BEACH', '1', '23C', '0mm', NULL, 22.369236, 114.080460, 46285, 1),
(38, 'Tong Fuk Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'South Lantau Road, Lantau Island', 3, 'ISLANDS', 'BEACH', '1', '19C', '0mm', NULL, 22.227856, 113.934776, 116430, 1),
(39, 'Trio Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Trio(Hebehaven), Sai Kung', 2, 'SAI KUNG', 'BEACH,BARBECUE SITES', '1', '19C', '0mm', NULL, 22.357710, 114.267883, 34975, 1),
(40, 'Turtle Cove Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'Tai Tam Road, Stanley', 1, 'SOUTHERN', 'BEACH,BARBECUE SITES', '1', '19C', '0mm', NULL, 22.232880, 114.223305, 35850, 1),
(41, 'Upper Cheung Sha Beach', 'The annual ranking of this beach was “Good” since EPD’s beach water quality monitoring programme in 1986. During a bathing season, the beach mostly achieves Grade 1, while sometimes it hovers to Grade 2. During and after periods of heavy rain, many beaches are likely to be more polluted than the beach grades suggest. Bathers should avoid swimming at beaches for up to three days after a storm or heavy rain.', 'South Lantau Road, Lantau Island', 3, 'ISLANDS', 'BEACH,BARBECUE SITES', '1', '19C', '0mm', NULL, 22.234104, 113.954109, 173080, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bookmark`
--

CREATE TABLE `bookmark` (
  `bookmark_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `beach_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `history_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `beach_id` int(11) NOT NULL,
  `visit_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE `userinfo` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `is_admin` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beachinfo`
--
ALTER TABLE `beachinfo`
  ADD PRIMARY KEY (`beach_id`),
  ADD KEY `beach_id` (`beach_id`);

--
-- Indexes for table `bookmark`
--
ALTER TABLE `bookmark`
  ADD PRIMARY KEY (`bookmark_id`),
  ADD KEY `user_id_const` (`user_id`),
  ADD KEY `beach_id_const` (`beach_id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`history_id`),
  ADD KEY `user_id_his_const` (`user_id`),
  ADD KEY `beach_id_his_const` (`beach_id`);

--
-- Indexes for table `userinfo`
--
ALTER TABLE `userinfo`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name` (`user_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beachinfo`
--
ALTER TABLE `beachinfo`
  MODIFY `beach_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `bookmark`
--
ALTER TABLE `bookmark`
  MODIFY `bookmark_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userinfo`
--
ALTER TABLE `userinfo`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookmark`
--
ALTER TABLE `bookmark`
  ADD CONSTRAINT `beach_id_const` FOREIGN KEY (`beach_id`) REFERENCES `beachinfo` (`beach_id`),
  ADD CONSTRAINT `user_id_const` FOREIGN KEY (`user_id`) REFERENCES `userinfo` (`user_id`);

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `beach_id_his_const` FOREIGN KEY (`beach_id`) REFERENCES `beachinfo` (`beach_id`),
  ADD CONSTRAINT `user_id_his_const` FOREIGN KEY (`user_id`) REFERENCES `userinfo` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
