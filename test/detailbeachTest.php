<?php
use PHPUnit\Framework\TestCase;
require_once(__DIR__ . '/../src/detailbeach.php');
class detailbeachTest extends TestCase {
    function testGetMapInfo() {
        $rc['beach_name'] = "Anglers' Beach";
        $rc['address'] = "13 milestone, Castle Peak Road - Sham Tseng, Tsuen Wan";
        $rc['district'] = "TSUEN WAN";
        $rc['lat'] = "22.364876";
        $rc['lng'] = "114.055931";
        $mapInfo = getBeachMapInfo($rc);
        $this->assertEquals("'Anglers\' Beach'", $mapInfo['beach_name']);
        $this->assertEquals("'13 milestone, Castle Peak Road - Sham Tseng, Tsuen Wan, Tsuen Wan'", $mapInfo['address']);
        $this->assertEquals('22.364876', $mapInfo['lat']);
        $this->assertEquals('114.055931', $mapInfo['lng']);
    }
}
?>