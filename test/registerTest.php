<?php
use PHPUnit\Framework\TestCase;
require_once(__DIR__ . '/../src/register.php');
class registerTest extends TestCase {
    function testCheckPassword() {
        $this->assertEquals(true, checkRegisterPassword('a1234567', 'a1234567'));
        $this->assertEquals(false, checkRegisterPassword('abcdefg', 'abcdef'));
    }
}
?>