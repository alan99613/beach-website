<?php
use PHPUnit\Framework\TestCase;
require_once(__DIR__ . '/../src/profile.php');
class profileTest extends TestCase {
    function testCheckPassword() {
        $this->assertEquals(true, checkPassword('1234', '1234'));
        $this->assertEquals(false, checkPassword('1111', '1234'));
    }
}
?>